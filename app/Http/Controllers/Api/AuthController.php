<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ApiUtilities;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Testing\Fluent\Concerns\Has;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|string',
            'password' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors(),
                'success' => FALSE
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        if(!Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password')])) {
            return response([
                'message' => 'Credenciales invalidas'
            ], Response::HTTP_UNAUTHORIZED);
        }

        /**@var User $user*/
        $user = Auth::user();

        $token = $user->createToken('token')->plainTextToken;

        return response([
            'user' => $user ,
            'extra' => [
                'token' => $token
            ]], Response::HTTP_OK);

    }

    public function logout(Request $request)
    {
        /**@var User $user*/
        $user = Auth::user();

        if($user->tokens()->delete()){
            return response([
                'success' => true
            ], Response::HTTP_OK);
        }else{
            return response([
                'success' => false
            ], Response::HTTP_REQUEST_TIMEOUT);
        };

    }
}
