<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use PHPUnit\TextUI\Exception;
use Spatie\Permission\Models\Role;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    public function index()
    {
        return view('users');
    }

    public function listRoles(Request $request)
    {
        try {
            $roles = Role::all();
            return response([
                'roles' => $roles,
                'success' => true
            ], Response::HTTP_OK);

        }catch (Exception $exception) {
            return response([
                'message' => $exception->getMessage(),
                'success' => false
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function list(Request $request)
    {
        /**@var User $user*/
        $user = Auth::user();

        try {

            if ($user->hasRole('admin')){
                $users = User::with('roles')->get();
            }

            if ($user->hasRole('seller')) {
                $users = User::role('client')->with('roles')->get();
            }
            return response([
                'users' => $users,
                'success' => true
            ], Response::HTTP_OK);

        }catch (Exception $exception) {
            return response([
                'message' => $exception->getMessage(),
                'success' => false
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $user = User::with('roles')->find($id);
            if (!$user) {
                return response([
                    'message' => "Id: $id Don't exists",
                    'success' => false
                ], Response::HTTP_BAD_REQUEST);
            }
            return response([
                'user' => $user,
                'success' => true
            ], Response::HTTP_OK);

        }catch (Exception $exception) {
            return response([
                'message' => $exception->getMessage(),
                'success' => false
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role = $request->get('role_id') ?? [];

        try {
            /**@var User $user*/
            $user = User::query()->create([
                'name' => $request->get('name'),
                'dni' => $request->get('dni'),
                'email' => $request->get('email'),
                'address' => $request->get('address'),
                'password' => Hash::make(env('UNIVERSAL_PASSWORD'))
            ]);

            if (!empty($role)) {
                $user->syncRoles($role);
            }

            return response([
                'user' => $user,
                'success' => true
            ], Response::HTTP_OK);

        }catch (\Exception $exception) {
            return response([
                'message' => $exception->getMessage(),
                'success' => false
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $role = $request->get('role_id') ?? [];

        try {
            /**@var User $user*/
            $user->update([
                'name' => $request->get('name'),
                'dni' => $request->get('dni'),
                'email' => $request->get('email'),
                'address' => $request->get('address'),
            ]);

            if (!empty($role)) {
                $user->syncRoles($role);
            }

            return response([
                'user' => $user,
                'success' => true
            ], Response::HTTP_OK);

        }catch (\Exception $exception) {
            return response([
                'message' => $exception->getMessage(),
                'success' => false
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {

        try {
            /**@var User $user*/
            $user->delete();
            return response([
                'message' => 'Usuario eliminado',
                'success' => true
            ], Response::HTTP_OK);

        }catch (\Exception $exception) {
            return response([
                'message' => $exception->getMessage(),
                'success' => false
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }


}
