import axios from 'axios';
import useToken from "./useToken";

const url = 'http://store-system.test/api/'
const user = {};
const token = useToken();
const headers = {
    'Authorization': `Bearer ${token}`,
};

user.listUsers = async () => {
    const urlEndpoint = url+'users/list';

    const res =  await axios.get(urlEndpoint, {headers})
        .then(response => { return response.data;})
        .catch(error => { return error; });
    return res;
};

user.listRoles = async () => {
    const urlEndpoint = url+'users/roles';
    const res = await axios.get(urlEndpoint, {headers})
        .then(response=> {return response.data })
        .catch(error=>{ return error; })
    return res;
}

user.get = async (id) => {
    if (!id) return ;
    const urlEndpoint = url+`users/${id}`;
    const res = await axios.get(urlEndpoint, {headers})
        .then(response=>{ return response.data })
        .catch(error => { return error })
    return res;

}

user.save = async (data) => {
    const urlEndpoint = url+'users';
    const res = await axios.post(urlEndpoint,data, {headers})
        .then(response=> {return response.data })
        .catch(error=>{ return error; })
    return res;
}


export default user;
