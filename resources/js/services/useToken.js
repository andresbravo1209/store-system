const useToken = () => {
    const token = localStorage.getItem('appjwt');
    if (token) return token;
    return null;
};

export default useToken;
