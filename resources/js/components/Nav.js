import React, { Component } from 'react';
import { Link } from "react-router-dom";
import {Button} from "@material-ui/core";
import useToken from "../services/useToken";
import axios from "axios";
import swal from "sweetalert";

const handleLogout = (e) => {
    e.preventDefault();

    const token = useToken();

    const headers = {
        'Authorization': `Bearer ${token}`,
    };

    axios.post(`/api/logout`, {}, {headers}).then(response => {
        const success = response.data.success;
        console.log(success);
        if (success) {
            localStorage.setItem('appjwt', null);
            localStorage.setItem('appUser', null);
            window.location = '/login';
        }}).catch(error => {
            //  Handling the response (Show an error notification etc)
        if (error.response) { // You can also check the status i.e 422
            if (error.response.status === 422){
                if (error.response.data.errors){
                    let msg = 'Error de validación';
                    const errors = error.response.data.errors;
                    msg += ': ' + Object.values(errors).join(' ');
                    swal({
                        title: "Ops!",
                        text: msg,
                           icon: "warning",
                    });
                }
                }else if (error.response.status === 408){
                    console.log(error.response.data.message);
                    swal({
                        title: "Ops!",
                        text: 'Ah ocurrido un error',
                        icon: 'error',
                    });
                }
            }
        })
};
const token = useToken();

export default class Nav extends Component {

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light rounded">
                <div className="collapse navbar-collapse" id="navbarsExample09">
                    {token !== 'null' ? <ul className="navbar-nav mr-auto">
                        <li className="nav-item mt-2">
                            <Button className="nav-link" onClick={handleLogout}>Cerrar sesión</Button>
                        </li>
                        <li className="nav-item">
                            <Link class="nav-link" to="/users">List  </Link>
                        </li>
                        <li className="nav-item">
                            <Link class="nav-link" to="/users/form">Create</Link>
                        </li>
                        <li className="nav-item">
                            <Link class="nav-link" to="/users/edit/5">Edit</Link>
                        </li>

                    </ul> : <ul className="navbar-nav mr-auto">
                        <li className="nav-item mt-2">
                            <a class="nav-link" href="/login">Inicia Sesión para continuar</a>
                        </li>
                    </ul> }
                </div>
            </nav>
        )
    }
}
