import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import Form from './users/Form';
import List from './users/List';
import Edit from './users/Edit';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import Nav from "./Nav";

const Main = () => {
    return (
        <Router>
            <main>
                <Nav/>
                <Switch>
                    <Route path="/users" exact component={List} />
                    <Route path="/users/form"  component={Form} />
                    <Route path="/users/edit/:id" component={Edit} />
                </Switch>
            </main>
        </Router>
    )
}

export default Main;

ReactDOM.render(<Main />, document.getElementById('main-users'));
