import React, { useEffect, useState } from 'react';
import userServices from '../../services/Users';
import {Link} from "react-router-dom";
import useToken from "../../services/useToken";
import axios from "axios";
import swal from "sweetalert";

const List = () => {

    const [users, setUsers] = useState([]);

    useEffect(() => {
        async function fetchUsersData(){
            const response = await userServices.listUsers();
            if (response.success){
                setUsers(response.users);
            }
        }
        fetchUsersData();
    }, [])

    const handleDelete = (event, id) => {
        event.preventDefault();

        if (confirm('Deseas borrar el usuario')) {

            const token = useToken();
            const headers = {
                'Authorization': `Bearer ${token}`,
            };

            axios.delete(`/api/users/${id}`, {headers}).then(response => {
                const success = response.data.success;
                if (success) {
                    swal({
                        title: "Exito!",
                        text: response.data.message,
                        icon: "success",
                    });
                }
            }).catch(error => {
                //  Handling the response (Show an error notification etc)
                if (error.response) { // You can also check the status i.e 422
                    if (error.response.status === 422){
                        if (error.response.data.errors){
                            let msg = 'Error de validación';
                            const errors = error.response.data.errors;
                            msg += ': ' + Object.values(errors).join(' ');
                            swal({
                                title: "Ops!",
                                text: msg,
                                icon: "warning",
                            });
                        }
                    }else if (error.response.status === 401){
                        console.log(error.response.data.message);
                        swal({
                            title: "Ops!",
                            text: error.response.data.message,
                            icon: "warning",
                        });
                    }
                }
            })
        }
    }

    return (
        <section>
            <table className="table">
                <thead className="thead-dark">
                <tr>
                    <th scope="col">Rol</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Email</th>
                    <th scope="col">Dirección</th>
                    <th scope="col">Documento</th>
                    <th scope="col">Opciones</th>
                </tr>
                </thead>
                <tbody>
                {users.length > 0 && users.map(user => (
                    <tr key={user.id}>
                        <th scope="row">{user.roles.length > 0 ? user.roles[0].name : 'Sin rol asignado'}</th>
                        <td>{user.name}</td>
                        <td>{user.email}</td>
                        <td>{user.address}</td>
                        <td>{user.dni}</td>
                        <td>
                            <Link to={`/users/edit/${user.id}`} class='btn btn-light'>Editar</Link>
                            <a onClick={e => handleDelete(e, user.id)} className="btn btn-danger"> Eliminar </a>
                        </td>
                    </tr>
                ))}
                </tbody>
            </table>
        </section>
    )
}

export default List;
