import React, { useEffect, useState } from 'react';
import userServices from "../../services/Users"
import axios from "axios";
import swal from "sweetalert";
import useToken from "../../services/useToken";

function Edit(props){

    const [ id, setId ] = useState(null);
    const [ name, setName ] = useState('');
    const [ email, setEmail ] = useState('');
    const [ address, setAddress ] = useState('');
    const [ dni, setDni ] = useState('');
    const [ rol, setRol ] = useState('');
    const [ selectedRol , setSelectRol ] = useState('');
    const [ listRol, setListRol ] = useState([]);

    useEffect(()=>{
        async function fetchDataUser(){
            let id = props.match.params.id;
            const res = await userServices.get(id);

            if (res.success) {
                console.log(res);
                const user = res.user
                setId(user.id)
                setName(user.name)
                setEmail(user.email)
                setAddress(user.address)
                setDni(user.dni)
                if (user.roles.length > 0) {
                    setRol(user.roles[0].name);
                    setSelectRol(user.roles[0].name)
                }
            }
            else {
                alert(res.message)
            }
        };
        fetchDataUser();

        async function fetchDataRol(){
            const res = await userServices.listRoles();
            setListRol(res.roles)
        }
        fetchDataRol();
    },[]);

    const handleChange = (e) => {
        console.log(e.target.value);
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        const token = useToken();
        const headers = {
            'Authorization': `Bearer ${token}`,
        };

        axios.put(`/api/users/${id}`, {
            name: name,
            dni: dni,
            address:address,
            email: email,
            role_id: [rol]
        }, {headers}).then(response => {
            const user = response.data.user;
            const success = response.data.success;

            if (user && success) {
                swal({
                    title: "Exito!",
                    text: 'Usuario acutalizado correctamente',
                    icon: "success",
                });
                window.location = '/users';
            }
        }).catch(error => {
            //  Handling the response (Show an error notification etc)
            if (error.response) { // You can also check the status i.e 422
                if (error.response.status === 422){
                    if (error.response.data.errors){
                        let msg = 'Error de validación';
                        const errors = error.response.data.errors;
                        msg += ': ' + Object.values(errors).join(' ');
                        swal({
                            title: "Ops!",
                            text: msg,
                            icon: "warning",
                        });
                    }
                }else if (error.response.status === 401){
                    console.log(error.response.data.message);
                    swal({
                        title: "Ops!",
                        text: error.response.data.message,
                        icon: "warning",
                    });
                }
            }
        })
    };

    return (
        <div>
            <h4>Editar</h4>
            <hr/>
            <div className="row">
                <div className="col-md-6 mb-3">
                    <label htmlFor="name">Nombre</label>
                    <input
                        id='name'
                        type="text"
                        class="form-control"
                        defaultValue={name}
                        onChange={e => (setName(e.target.value))}
                    />
                </div>
            </div>
            <div className="row">
                <div className="col-md-6 mb-3">
                    <label htmlFor="email">Email</label>
                    <input
                        id='email'
                        type="email"
                        className="form-control"
                        placeholder="you@example.com"
                        defaultValue={email}
                        onChange={e => (setEmail(e.target.value))}
                    />
                </div>
            </div>
            <div className="row">
                <div className="col-md-6 mb-3">
                    <label htmlFor="address">Dirección</label>
                    <input
                        id='address'
                        type="text"
                        className="form-control"
                        placeholder="1234 Main St"
                        onChange={e => (setAddress(e.target.value))}
                        defaultValue={address}
                    />
                </div>
            </div>
            <div className="row">
                <div className="col-md-6 mb-3">
                    <label htmlFor="address">Documento</label>
                    <input
                        id='dni'
                        type="text"
                        className="form-control"
                        placeholder="123467890"
                        defaultValue={dni}
                        onChange={e => (setDni(e.target.value))}
                    />
                </div>
            </div>
            <div className="row">
                <div className="col-md-6 mb-3">
                    <label htmlFor="dni">Rol</label>
                    <select id="inputState" className="form-control" value={rol} onChange={e => {setRol(e.target.value)}}>
                        {listRol.map(rol =>{ return(<option key={rol.id} value={rol.name}>{rol.name}</option>)})}
                    </select>
                </div>
            </div>
            <div className="row">
                <div className="col-md-6 mb-3">
                    <button onClick={handleSubmit} className="btn btn-primary btn-block" type="submit">Guardar</button>
                </div>
            </div>
        </div>
    )
};

export default Edit;
