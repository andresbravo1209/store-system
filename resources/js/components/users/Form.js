import React, { useEffect, useState  } from 'react';
import userServices from "../../services/Users"
import swal from "sweetalert";

function Form(){

    const [ name, setName ] = useState(null);
    const [ email, setEmail ] = useState(null);
    const [ address, setAddress ] = useState(null);
    const [ dni, setDni ] = useState(null);
    const [ rol, setRol ] = useState(null);
    const [ roles, setRoles ] = useState([]);

    useEffect(() => {
        async function fetchDataRol() {
            // load data from API
            const res = await userServices.listRoles();
            setRoles(res.roles)
        }
        fetchDataRol();
    },[]);

    const saveEmployee = async () => {
        const data = {
            name, email, address, dni, role_id: [rol]
        }
        const res = await userServices.save(data);
        if (res.success) {
            swal({
                title: "Exito!",
                text: 'Usuario creado correctamente',
                icon: "success",
            });
            window.location = '/users';
        }
        else {
            alert(res.success)
        }
    }

    return(
        <div>
            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="firstName">Nombre del usuario </label>
                    <input type="text" class="form-control" placeholder="Name"
                           onChange={event => setName(event.target.value)} />
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" placeholder="you@example.com"
                           onChange={event=>setEmail(event.target.value)} />
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="address">Address</label>
                    <input type="text" class="form-control" placeholder="1234 Main St"
                           onChange={(event)=>setAddress(event.target.value)} />
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="phone">Documento </label>
                    <input type="text" class="form-control" placeholder="123467890"
                           onChange={(event)=>setDni(event.target.value)}  />
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="phone">Rol </label>
                    <select id="inputState" class="form-control" onChange={event => setRol(event.target.value)}>
                        <option selected>Choose...</option>{
                            roles.map(rol =>{ return(<option value={rol.name}>{rol.name}</option>)}) }
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 mb-3">
                    <button class="btn btn-primary btn-block" type="submit"
                            onClick={()=>saveEmployee()}>Save 2</button>
                </div>
            </div>
        </div>
    )
}

export default Form;
