import React, {useState} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import ReactDOM from "react-dom";
import axios from "axios";
import swal from 'sweetalert';

const useStyles = makeStyles((theme) => ({
    root: {
        height: '100vh',
    },
    image: {
        backgroundImage: 'url(https://source.unsplash.com/random)',
        backgroundRepeat: 'no-repeat',
        backgroundColor:
            theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    },
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const  Login = () => {

    const classes = useStyles();

    const [initialValues, setInitialValues] = useState({
        email: '',
        password: '',
    });

    const handleSubmit = (e) => {

        e.preventDefault();
        axios.post('/api/login', initialValues).then(response => {
            const user = response.data.user;
            const extra = response.data.extra;

            if (extra && extra.token) {
                const { name, email, phone, address } = user;
                const dataUser = {
                    name,
                    phone,
                    email,
                    address
                };

                localStorage.setItem('appjwt',extra.token);
                localStorage.setItem('appUser', JSON.stringify(dataUser));
                window.location = '/users';
            }
        }).catch(error => {
            //  Handling the response (Show an error notification etc)
            if (error.response) { // You can also check the status i.e 422
                if (error.response.status === 422){
                    if (error.response.data.errors){
                        let msg = 'Error de validación';
                        const errors = error.response.data.errors;
                        msg += ': ' + Object.values(errors).join(' ');
                        swal({
                            title: "Ops!",
                            text: msg,
                            icon: "warning",
                        });
                    }
                }else if (error.response.status === 401){
                    console.log(error.response.data.message);
                    swal({
                        title: "Ops!",
                        text: error.response.data.message,
                        icon: "warning",
                    });
                }
            }
        })
    };

    const handleChange = (e) => {
        const key = e.target.id;
        const value = e.target.value

        setInitialValues(values => ({
            ...values,
            [key]: value,
        }))
    }
    return (
        <Grid container component="main" className={classes.root}>
            <CssBaseline />
            <Grid item xs={false} sm={4} md={7} className={classes.image} />
            <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Sign in
                    </Typography>
                    <form className={classes.form} noValidate>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Email Address"
                            name="email"
                            autoFocus
                            onChange={handleChange}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            onChange={handleChange}
                        />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onClick={handleSubmit}
                        >
                            Iniciar Sesión
                        </Button>

                    </form>
                </div>
            </Grid>
        </Grid>
    )
}

export default Login;

if (document.getElementById('login-container')) {
    ReactDOM.render(<Login />, document.getElementById('login-container'));
}


