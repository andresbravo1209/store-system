<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Sistema de clientes</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <style> .navbar{margin-bottom: 20px;} </style>
</head>
<body>
<noscript>You need to enable JavaScript to run this app.</noscript>
<div id="login-container"></div>
<script src="{{ mix('admin/auth/login/Login.js') }}"></script>
</body>
</html>
