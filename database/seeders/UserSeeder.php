<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**@var User $admin*/
        $admin = User::factory()->create([
            'email' => 'admin@store.com'
        ]);

        /**@var User $seller1*/
        $seller1 = User::factory()->create([
            'email' => 'seller1@store.com'
        ]);

        /**@var User $seller2*/
        $seller2 = User::factory()->create([
            'email' => 'seller2@store.com'
        ]);

        $admin->syncRoles('admin');
        $seller1->syncRoles('seller');
        $seller2->syncRoles('seller');


        // Clients
        /** @var User */
        [$user1, $user2, $user3, $user4, $user5] = User::factory(5)->create([]);

        $user1->syncRoles('client');
        $user2->syncRoles('client');
        $user3->syncRoles('client');
        $user4->syncRoles('client');
        $user5->syncRoles('client');
    }
}
