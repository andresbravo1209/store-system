
## Sistema de usuarios

La siguiente aplicacion contiene unos usuarios por defecto, con roles

- administrador,  'email' => 'admin@store.com'
- vendedor, 'email' => 'seller1@store.com' | 'email' => 'seller2@store.com'
- cliente

con clave asignada por defecto como: @123456

### 1. Crear la base de datos
- Nombre: store-system

### 2. Configurar credenciales en el archivo .env y copiar archivo .env.example

### 3. Instalar el proyecto
- composer install && php artisan key:generate

### 4. Instalar dependencias npm
- npm i && npm run dev

### 4. Correr migraciones de usuarios por defecto

- php artisan migrate
- php artisan db:seed --class=UserSeeder

###  Request validators faltante en parte del proyecto.


