<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [AuthController::class, 'login']);

Route::middleware('auth:sanctum')->group( function () {

    Route::get('/users/roles', [UserController::class, 'listRoles'])->middleware(['role:admin|seller']);
    Route::get('/users/list', [UserController::class, 'list'])->middleware(['role:admin|seller']);
    Route::get('/users/{id}', [UserController::class, 'show'])->middleware(['role:admin|seller']);
    Route::post('/users', [UserController::class, 'store'])->middleware(['role:admin|seller']);
    Route::put('/users/{user}', [UserController::class, 'update'])->middleware(['role:admin|seller']);
    Route::delete('/users/{user}', [UserController::class, 'destroy'])->middleware(['role:admin|seller']);

    Route::post('logout', [AuthController::class, 'logout']);

});
